
var turn = 1
var items;
var boardsize;
var winsize;
var list;
var xs;
var os;

function validate(id){
    //check L/R
    list[id] =  items[id].innerHTML;
    var rows = [];

    for(i=0; i <= list.length-boardsize; i=parseInt(i)+parseInt(boardsize)){ //i=parseInt(i+boardsize) makes i move down to the next row
        rows.push(list.slice(i, parseInt(i)+parseInt(boardsize))); //this takes the next two in front of i and puts it into the appropraite set
    }
    
    for(i=0; i < rows.length; i++){
        if(parseInt(rows[i].filter(x => x == "X").length) == parseInt(winsize)){
            alert("Congradulations, player " + items[id].innerHTML);
            break;
        }
        
        if(parseInt(rows[i].filter(x => x == "O").length) == parseInt(winsize)){
            alert("Congradulations, player " + items[id].innerHTML);
            break;
        }
    }
            
    //check U/D
    var cols = new Array(boardsize).fill([]);
    var x = 0;
    var temp = [];
    
    for(i=0; i < boardsize; i++){
        for(x=i; x < boardsize*boardsize; x=x+parseInt(boardsize)){
            temp.push(items[x].innerHTML);
        }

        cols[i] = temp;
        temp = []
    }
    console.log("-----------------")

    for(i=0; i < cols.length; i++){
        if(parseInt(cols[i].filter(x => x == "X").length) == parseInt(winsize)){
            alert("Congradulations, player " + items[id].innerHTML);
            break;
        }
        
        if(parseInt(rows[i].filter(x => x == "O").length) == parseInt(winsize)){
            alert("Congradulations, player " + items[id].innerHTML);
            break;
        }
    }

    //check diag
    var diags = new Array(boardsize).fill([]);
    var x = 0;
    var temp = [];
    
    for(i=0; i < boardsize; i++){
        for(x=i; x < boardsize*boardsize; x=x+parseInt(boardsize)+1){
            temp.push(items[x].innerHTML);
        }

        diags[i] = temp;
        console.log(i + ": " + diags[i])
        temp = []
    }
    console.log("-----------------")

    for(i=0; i < diags.length; i++){
        if(parseInt(diags[i].filter(x => x == "X").length) == parseInt(winsize)){
            alert("Congradulations, player " + items[id].innerHTML);
            break;
        }
        
        if(parseInt(diags[i].filter(x => x == "O").length) == parseInt(winsize)){
            alert("Congradulations, player " + items[id].innerHTML);
            break;
        }
    }
}

function code(id){
    if(turn == 1) {
        document.getElementById(id).innerHTML = "X";
        turn = 2;
        validate(id)
    } else if (turn == 2) {
        document.getElementById(id).innerHTML = "O";
        turn = 1;
        validate(id)
    }
}

function reset(){
    document.getElementById('submit').disabled = false;
    document.getElementById('reset').disabled = true;
    document.getElementById('bsize').value = "";
    document.getElementById('wsize').value = "";
    document.getElementsByClassName('grid-container')[0].remove();
    turn = 1;
}

function generate(bsize, wsize){
    document.getElementById('submit').disabled = true;
    document.getElementById('reset').disabled = false;
    list = Array(bsize*bsize).map(x => 0); //fills list with 0's

    var container = document.createElement('div');
    container.className = 'grid-container';

    boardsize = bsize;
    winsize = wsize;

    if((bsize >= 3 & wsize >= 3) & (bsize >= wsize)){
        container.style.gridTemplateColumns = "repeat(" + bsize + ", 1fr)"
        document.body.appendChild(container);
    
        for(var i=0; i<bsize*bsize; i++){
            var div = document.createElement('div');
            div.className = 'grid-item';
            div.id = i;
            container.appendChild(div);
        }
        items = document.querySelectorAll(".grid-item")
    
        for(let i=0; i < items.length; i++) { 
            items[i].addEventListener("click", function() {
                code(i)
            })
        }
    } else {
        bsize = 3;
        wsize = 3;
        generate(bsize, wsize)
        document.getElementById('bsize').value = "3";
        document.getElementById('wsize').value = "3";
    }
}

